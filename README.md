# README #  
  
I wrote this for a code project CMS system. Simply include the function somewhere in your code/project and use it like the following:  
  
/**  
 * Returns pagination html  
 *  
 * @param string - $params url get params  
 * @param int - $tPages total number of pages  
 * @param int - current page/content id  
 * @param boolean (true/false) - align right of page (default: false (left))  
 * @param int - total of numbered crumbs to show  
 * @return mixed/html  
 */  
  
<?php echo returnPagination(@$this->params, $this->totalPages, $this->page, false, $this->contentListResults); ?>  
