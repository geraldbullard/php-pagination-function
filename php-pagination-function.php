<?php
/**
 * Returns pagination html
 *
 * @param string - $params url get params
 * @param int - $tPages total number of pages
 * @param int - current page/content id
 * @param boolean (true/false) - align right of page (default: false (left))
 * @param int - total of numbered crumbs to show
 * @return mixed/html
 */
function returnPagination($params = '', $tPages = '', $page = 1, $right = '', $count = '', $crumbs = 5)
{
    $pageStr = '';
    $paramStr = '';
    $isPage = false;
    $pHTML = '';
    if ($count > 0) {
        if (isset($_GET['page']) && $_GET['page'] != '') {
            $pageStr .= 'page=' . $_GET['page'];
            $isPage = true;
        }
        if ($params != '') {
            foreach ($params as $key => $param) {
                $paramStr .= '&' . $key . '=' . urlencode($param);
            }
        }
        $max = min($tPages, $crumbs);
        $limit = ((int)floor($max / 2));
        $leading = $limit;
        for ($x = 0; $x < $limit; ++$x) {
            if ($page === ($x + 1)) {
                $leading = $x;
                break;
            }
        }
        for ($x = $tPages - $limit; $x < $tPages; ++$x) {
            if ($page === ($x + 1)) {
                $leading = $max - ($tPages - $x);
                break;
            }
        }
        $trailing = $max - $leading - 1;
        if (($tPages - $page) < $trailing) {
            $leading = ($leading + ($leading - ($tPages - $page)));
            $trailing = ($tPages - $page);
        }
        $pHTML .= '<ul class="pagination' . ($right ? ' pull-right' : '') . '">';
        // first
        $pHTML .= '    <li class="paginate_button previous' . ($page <= 1 ? ' disabled' : '') . '" title="First Page">';
        $pHTML .= '        <a href="' . ($page <= 1 ? 'javascript:;' : '/' . $_GET['url'] . '?page=1' . $paramStr) .
            '"><i class="fa fa-fast-backward"></i></a>';
        $pHTML .= '    </li>';
        // previous
        $pHTML .= '    <li class="paginate_button previous' . ($page <= 1 ? ' disabled' : '') . '">';
        $pHTML .= '        <a href="' . ($page >= 2 ? '/' . $_GET['url'] . '?page=' . ($page - 1) . $paramStr : 'javascript:;') .
            '" title="Previous Page"><i class="fa fa-step-backward"></i></a>';
        $pHTML .= '    </li>';
        // leading pages
        for ($x = 0; $x < $leading; ++$x) {
            if (($page + $x - $leading) != 0) {
                $pHTML .= '    <li class="paginate_button">';
                $pHTML .= '        <a href="' . '/' . $_GET['url'] . '?page=' . ($page + $x - $leading) . $paramStr . '">';
                $pHTML .= ($page + $x - $leading);
                $pHTML .= '        </a>';
                $pHTML .= '    </li>';
            }
        };
        // current page
        $pHTML .= '    <li class="paginate_button active"><a href="javascript:;">' . $page . '</a></li>';
        // trailing pages
        for ($x = 0; $x < $trailing; ++$x) {
            if (($page + $x + 1) <= $tPages) { // don't show if higher than total pages
                $pHTML .= '    <li class="paginate_button">';
                $pHTML .= '        <a href="' . '/' . $_GET['url'] . '?page=' . ($page + $x + 1) . $paramStr . '">';
                $pHTML .= ($page + $x + 1);
                $pHTML .= '        </a>';
                $pHTML .= '    </li>';
            }
        };
        // next
        $pHTML .= '   <li class="paginate_button next' . ($page >= $tPages ? ' disabled' : '') . '">';
        $pHTML .= '       <a href="' . ($page >= $tPages ? 'javascript:;' : '/' . $_GET['url'] . '?page=' . ($page + 1)) . $paramStr .
            '" title="Next Page"><i class="fa fa-step-forward"></i></a>';
        $pHTML .= '    </li>';
        // last
        $pHTML .= '   <li class="paginate_button next' . ($page >= $tPages ? ' disabled' : '') . '">';
        $pHTML .= '       <a href="' . ($page == $tPages ? 'javascript:;' : '/' . $_GET['url'] . '?page=' . $tPages . $paramStr) .
            '" title="Last Page"><i class="fa fa-fast-forward"></i></a>';
        $pHTML .= '    </li>';
        $pHTML .= '</ul>';
    }

    return $pHTML;
}